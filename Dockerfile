FROM python:3.8-buster
LABEL authors="Kheir Eddine FARFAR <kheir.farfar@tib.eu>"

# Upgrade pip and install poetry
RUN pip install --upgrade pip && pip install poetry

WORKDIR /app

# Copy only requirements to cache them in docker layer
COPY poetry.lock pyproject.toml /app/

# Project initialization: disable crearing a virtual environment
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi --no-dev

RUN python3 -m nltk.downloader stopwords
RUN python3 -m nltk.downloader wordnet

# Creating folders, and files for a project:
COPY . /app

RUN python3 check_entities.py

EXPOSE 8072

CMD ["flask", "run", "--port=8072", "--host=0.0.0.0"]
