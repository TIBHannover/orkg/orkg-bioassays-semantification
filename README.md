# ORKG Bioassays Semantification & Predicates Recommendation

Semantification system that automatically semantify bioassay data in ORKG.
Recommendation system that recommends ORKG predicates given a paper.

# Notes

- The repository uses [Git LFS](https://git-lfs.github.com/) to track large files (e.g model), if you don't have it on your machine, you need to download those files manually:

  - `models/assay-semantification-k-means-model.pkl` <small>(332 MB)</small>
  - `models/predicates-recommendation-k-means-model.pkl` <small>(3.58 GB) </small>
  - `models/tfidf-assay-vectorizer-model.pkl` <small>(4.20 MB) </small>
  - `models/tfidf-predicates-vectorizer-model.pkl` <small>(11.3 MB) </small>


# How it works:

## Bioassays Semantification
This service provides an API endpoint `/bioassay` that accepts requests via `POST` method, a JSON object with the attribute `bioassay` that contains the text of Bioassays, and it returns the Bioassays Semantification.

The script `check_entities.py` checks the existence of a predefined set of entities (resources,predicates,classes, and statements) from the [BAO Ontology](https://bioportal.bioontology.org/ontologies/BAO) in the provided backend API endpoint (`ORKG_SERVER_URL` in .env) and it creates the entities in case of no existence. After that, it saves the result in pickle files (ids/predicates.pickle and resources.pickle)

**Statistics about the predefined entities**:

Predicates: 55

Classes: 663

Resources: 1559

Statements: 341

### Example:

> curl --location --request POST 'http://127.0.0.1:8072/bioassay/' \
> --header 'Content-Type: application/json' \
> --data-raw '{ "bioassay": "Currently there are no small molecule tools to investigate the biological functions of apelin and its receptor. Apelin is the endogenous peptide ligand for the G-protein coupled receptor (GPCR) APJ (angiotensin II receptor-like 1, AGTRL-1 and APLNR). Until the discovery of apelin, APJ was an orphan GPCR. APJ is coupled to Gai, and has been shown in cell culture to inhibit adenylate cyclase. The APJ gene encodes a receptor that most closely resembles the angiotensin receptor AT1. However, the APJ receptor does not bind angiotensin II. Underscoring the emerging importance of the apelin/APJ system, recent studies have shown that apelin reduces the extent of atherosclerotic lesions in ApoE-/- mice, and opposes the development of abdominal aortic aneurysms. "}'

**Response**:

```json
{
  "labels": {
    "DNA construct": "Expressing the Apelin receptor and beta-arrestin (DiscoveRx #93-0250C2)",
    "assay measurement type": "endpoint assay",
    "has assay control": [
      "negative control",
      "positive control"
    ]
  },
  "properties": {
    "P34565": "DNA construct",
    "P34258": "assay measurement type",
    "P34555": "has assay control"
  },
  "resources": {
    "R51223": "endpoint assay",
    "R72448": "Expressing the Apelin receptor and beta-arrestin (DiscoveRx #93-0250C2)",
    "R71734": "negative control",
    "R71741": "positive control"
  }
}
```

## Predicates Recommendation
This service provides an API endpoint `/predicates` that accepts requests via `GET` method, with the query parameter `title` for the paper title and optionally `doi` for the paper DOI.

Calling the service will produce a JSON object as a response with the recommended ORKG predicates.

### Example:

> curl --location --request GET 'http://127.0.0.1:8072/predicates/?doi=10.1145/2894784.2894795&title=UCFrame: A Use Case Framework for Crowd-Centric Requirement Acquisition' \
> --header 'Content-Type: application/json' \

**Response**:

```json
[
    {
        "id": "P36056",
        "label": "re activities with crowd involvement"
    },
    {
        "id": "P36056;P36098",
        "label": "re activities with crowd involvement;performed by"
    },
    {
        "id": "P36056;P36095",
        "label": "re activities with crowd involvement;elicited information"
    },
    {
        "id": "P36056;P36100",
        "label": "re activities with crowd involvement;run-time purpose"
    },
    {
        "id": "P36056;P36096",
        "label": "re activities with crowd involvement;used models"
    },
    {
        "id": "P36057",
        "label": "utilities in crowdre"
    },
    {
        "id": "P36057;P37599",
        "label": "utilities in crowdre;level of knowledge, skills, and expertise"
    },
    {
        "id": "P36057;P30017",
        "label": "utilities in crowdre;role"
    },
    {
        "id": "P36057;P37598",
        "label": "utilities in crowdre;scale"
    },
    {
        "id": "P36057;P36097",
        "label": "utilities in crowdre;incentive/motivation"
    },
    {
        "id": "P36057;P36099",
        "label": "utilities in crowdre;media/channel"
    },
    {
        "id": "P36057;type",
        "label": "utilities in crowdre;type"
    },
    {
        "id": "P36057;P2003",
        "label": "utilities in crowdre;best complexity"
    },
    {
        "id": "P36057;P37596",
        "label": "utilities in crowdre;complexity"
    },
    {
        "id": "P36057;P4012",
        "label": "utilities in crowdre;type"
    }
]
```
Note that the semicolon ``;`` in a predicate object delimits two predicate resources and denotes a path of predicates in the ORKG.

# Setup

- Create your `.env` file, see `.env.example` for reference.

The **environment variables** , that you need to provide, are as follows:

| Variable        | Description                                                                                                                        |
| --------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| FLASK_APP       | Used to specify how to load the application for the 'flask' command.                                                               |
| FLASK_ENV       | 'development' or 'production' and it's used to indicate to Flask, extensions, and other programs what context Flask is running in. |
| FLASK_DEBUG     | Whether debug mode is enabled. **Do not enable debug mode when deploying in production.**                                          |
| **ORKG API**    |                                                                                                                                    |
| ORKG_SERVER_URL | ORKG API endpoint (put you local backend API URL if you are using a local instance)                                                |

## Using docker-compose

Run `docker-compose up -d` and you are ready to go. The application can be accessed via [http://localhost:8072/](http://localhost:8072/)

## Manual setup

This project uses [poetry](https://python-poetry.org/) to manage the dependencies.

- After installing poetry, install the project itself:

```
poetry install
```

- Run the initialization script `check_entities.py`:

```
poetry run dotenv run python ./check_entities.py
```

- And launch the server with:

```
poetry run flask run --port=8072 --host=0.0.0.0
```

- The application can be accessed via [http://localhost:8072/](http://localhost:8072/) .
