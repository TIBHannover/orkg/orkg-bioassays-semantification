from marshmallow import Schema, validate
from webargs import fields


class BioassayPostParams(Schema):

    bioassay = fields.String(
        required=True, validate=validate.Length(min=1), location="json")
