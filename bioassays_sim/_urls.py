from flask import Blueprint
from .views import BioassaysSimAPI

bioassays_sim_blueprint = Blueprint("bioassay", __name__)

api_bioassay = BioassaysSimAPI.as_view("bioassay")

bioassays_sim_blueprint.add_url_rule(
    "/bioassay/", view_func=api_bioassay, methods=["POST"]
)
