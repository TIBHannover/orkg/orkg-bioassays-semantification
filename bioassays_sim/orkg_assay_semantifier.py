import sys
import pickle
from sklearn.cluster import KMeans
from text_preprocessor import *
from sklearn.feature_extraction.text import TfidfVectorizer
import re
from utils import append_value


with open("./bioassays_sim/ids/predicates.pickle", "rb") as handle:
    orkg_predicates = pickle.load(handle)
with open("./bioassays_sim/ids/resources.pickle", "rb") as handle:
    orkg_resources = pickle.load(handle)


def tf_idf_vectorize_data(text):
    preprocessed_text_arr = text_process(text)
    preprocessed_text_str = " ".join(preprocessed_text_arr)
    tfidfconvert = pickle.load(open("./models/tfidf-assay-vectorizer-model.pkl", "rb"))
    text_vectorized = tfidfconvert.transform([preprocessed_text_str])
    return text_vectorized


def get_cluster(text):
    text_vectorized = tf_idf_vectorize_data(text)

    kmeans = pickle.load(open("./models/assay-semantification-k-means-model.pkl", "rb"))
    cluster = kmeans.predict(text_vectorized)
    return cluster[0]


def semantify(cluster, orkg_predicates, orkg_resources):
    labels_object = {}
    predicates_object = {}
    resources_object = {}
    labels_file = open("./labels/clusters_output_700/" + str(cluster) + ".labels", "r")
    lines = labels_file.readlines()
    labels_file.close()
    for line in lines:
        line = line.strip()
        toks = line.split("\t")
        text = toks[0]
        frequency = int(toks[1])
        # this parameter can be adjusted to a higher frequency (at most 10) for fewer predicted labels
        if frequency >= 1:
            if "||" in text:
                # line with nested statements
                # they are not returned as objects because the nested statements are precreated
                # only the top-level node is returned
                labels = text.split("||")
                label_parts = labels[0].split(" ; # ")[1].split('" -> "')
            else:
                label_parts = text.split(" ; # ")[1].split('" -> "')

            lhs = label_parts[0][1:]
            if lhs in orkg_predicates:
                predicates_object[orkg_predicates[lhs]] = lhs
            while lhs[0] == '"':
                lhs = lhs[1:]
            while lhs[len(lhs) - 1] == '"':
                lhs = lhs[0 : len(lhs) - 1]

            rhs = label_parts[1][0 : len(label_parts[1]) - 1]
            while rhs[0] == '"':
                rhs = rhs[1:]
            while rhs[len(rhs) - 1] == '"':
                rhs = rhs[0 : len(rhs) - 1]

            if rhs in orkg_resources:
                resources_object[orkg_resources[rhs]] = rhs

            append_value(labels_object, lhs, rhs)

    return predicates_object, resources_object, labels_object


def semantify_text(text):
    cluster_number = get_cluster(text)
    predicates_object, resources_object, labels_object = semantify(
        cluster_number, orkg_predicates, orkg_resources
    )
    # if the text is double-quoted, it will not work since the text includes double quotes
    return {
        "properties": predicates_object,
        "resources": resources_object,
        "labels": labels_object,
    }
