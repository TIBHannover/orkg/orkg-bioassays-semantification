from flask import jsonify, request, redirect, url_for
from flask.views import MethodView
from ._params import BioassayPostParams
from .orkg_assay_semantifier import semantify_text
from utils import use_args_with


class BioassaysSimAPI(MethodView):
    @use_args_with(BioassayPostParams)
    def post(self, reqargs):
        return jsonify(semantify_text(text=reqargs.get("bioassay")))
