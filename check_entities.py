import os
import pickle
from orkg import ORKG
from progress.bar import Bar

host = (
    os.environ["ORKG_SERVER_URL"]
    if "ORKG_SERVER_URL" in os.environ
    else "https://www.orkg.org/orkg/"
)
orkg = ORKG(host)


def precheck_orkg_entities():
    """
    Reads the ORKG resource and predicate identifiers
        If it doesn't exist, it creates them
    """
    orkg_predicates = read_file_in_dictionary(
        "./bioassays_sim/data/orkg-predicates.txt"
    )
    for label in Bar("Checking predicates").iter(orkg_predicates):
        orkg_predicates[label] = create_or_find_predicate(label)

    orkg_classes = read_file_in_dictionary_triples(
        "./bioassays_sim/data/orkg-classes.txt"
    )
    for label in Bar("Checking classes").iter(orkg_classes):
        (uri, id) = orkg_classes[label]
        orkg_classes[label] = (uri, create_or_find_class(label, uri))

    orkg_resources = read_file_in_dictionary("./bioassays_sim/data/orkg-resources.txt")
    for label in Bar("Checking resources").iter(orkg_resources):
        if label in orkg_classes:
            (uri, class_id) = orkg_classes[label]
        else:
            class_id = ""
        orkg_resources[label] = create_or_find_resource(label, class_id)
    return orkg_predicates, orkg_resources


def precheck_orkg_statements(file, orkg_predicates, orkg_resources):
    d = {}
    with open(file) as f:
        bar = Bar("Checking statements", max=341)
        for line in f:
            line = line.strip()
            (
                subj_res_label,
                subj_res_id,
                pred_label,
                pred_id,
                obj_res_label,
                obj_res_id,
            ) = line.split("\t")

            if orkg_resources[subj_res_label] != subj_res_id:
                subj_res_id = orkg_resources[subj_res_label]
            if orkg_resources[obj_res_label] != obj_res_id:
                obj_res_id = orkg_resources[obj_res_label]
            if orkg_predicates[pred_label] != pred_id:
                pred_id = orkg_predicates[pred_label]

            create_or_find_nested_statements(subj_res_id, pred_id, obj_res_id)

            d[subj_res_id] = (pred_id, obj_res_id)
            bar.next()
        bar.finish()
    return d


def create_or_find_predicate(label: str):
    """
    Retrieves the ID of the first matching entity if already in ORKG otherwise create it
    """
    found = orkg.predicates.get(q=label, exact=True).content
    if len(found) > 0:
        predicate_id = found[0]["id"]
    else:
        predicate_id = orkg.predicates.add(label=label).content["id"]
    return predicate_id


def create_or_find_class(label: str, uri: str):
    found = orkg.classes.get_all(q=label, exact=True).content
    if len(found) > 0:
        class_id = found[0]["id"]
    else:
        response = orkg.classes.add(label=label, uri=uri)
        class_id = ""
        if response.status_code == 400:
            string = response.content["errors"][0]["message"]
            tokens = string.split(" ")
            last_token = tokens[len(tokens) - 1]
            class_id = last_token[0 : len(last_token) - 1]
            orkg.classes.update(id=class_id, label=label)
        elif response.status_code == 200:
            class_id = response.content["id"]
    return class_id


def create_or_find_resource(label: str, class_id: str):
    if class_id != "":
        found = orkg.classes.get_resource_by_class(
            class_id, q=label, exact=True
        ).content
        if len(found) > 0:
            resource_id = found[0]["id"]
        else:
            resource_id = orkg.resources.add(label=label, classes=[class_id]).content[
                "id"
            ]
    else:
        found = orkg.resources.get(q=label, exact=True).content
        if len(found) > 0:
            resource_id = found[0]["id"]
        else:
            resource_id = orkg.resources.add(label=label).content["id"]

    return resource_id


def create_or_find_nested_statements(subj_id, pred_id, obj_id):
    """
    Retrieves the ID if already in ORKG otherwise create it
    """
    found = orkg.statements.get_by_subject(subject_id=subj_id).content

    statement_found = False
    if len(found) > 0:
        for item in found:
            if item["predicate"]["id"] == pred_id and item["object"]["id"] == obj_id:
                statement_found = True

    if not statement_found:
        orkg.statements.add(subject_id=subj_id, predicate_id=pred_id, object_id=obj_id)


def read_file_in_dictionary(file):
    d = {}
    f = open(file, "r")
    for line in f:
        line = line.strip()
        (key, val) = line.split("\t")
        d[key] = val
    f.close()
    return d


def read_file_in_dictionary_triples(file):
    d = {}
    with open(file) as f:
        for line in f:
            line = line.strip()
            (key, uri, val) = line.split("\t")
            d[key] = (uri, val)
    return d


if __name__ == "__main__":
    # Check the existence of the entities
    orkg_predicates, orkg_resources = precheck_orkg_entities()
    with open("./bioassays_sim/ids/predicates.pickle", "wb") as handle:
        pickle.dump(orkg_predicates, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open("./bioassays_sim/ids/resources.pickle", "wb") as handle:
        pickle.dump(orkg_resources, handle, protocol=pickle.HIGHEST_PROTOCOL)
    # Check the nested resource statements
    precheck_orkg_statements(
        "./bioassays_sim/data/statements.txt", orkg_predicates, orkg_resources
    )
