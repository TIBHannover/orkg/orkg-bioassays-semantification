from flask import Blueprint
from .views import PredicatesSimAPI

predicates_sim_blueprint = Blueprint("predicates", __name__)

api_predicates = PredicatesSimAPI.as_view("predicates")

predicates_sim_blueprint.add_url_rule(
    "/predicates/", view_func=api_predicates, methods=["GET"]
)
