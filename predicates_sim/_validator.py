from common.errors import ValidationError


class Validator:

    @staticmethod
    def validate_request(query_parameters):
        if 'title' not in query_parameters or not query_parameters['title']:
            raise ValidationError('"title" must be specified as query parameter')
