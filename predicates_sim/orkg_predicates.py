import pandas as pd
import numpy as np

from text_preprocessor import text_process
from utils import IO


class PredicatesRecommender:
    __instance = None
    VECTORIZER_PATH = './models/tfidf-predicates-vectorizer-model.pkl'
    CLUSTERING_MODEL_PATH = './models/predicates-recommendation-k-means-model.pkl'
    TRAINING_SET_PATH = './predicates_sim/data/training_set.json'
    COMPARISONS_PREDICATES_PATH = './predicates_sim/data/comparisons_predicates.json'

    def __init__(self):
        if PredicatesRecommender.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            PredicatesRecommender.__instance = self

        self.vectorizer = IO.read_pickle(self.VECTORIZER_PATH)
        self.model = IO.read_pickle(self.CLUSTERING_MODEL_PATH)
        self.train_df = self.read_training_data()
        self.comparisons = IO.read_json(self.COMPARISONS_PREDICATES_PATH)

    @staticmethod
    def get_instance():
        if PredicatesRecommender.__instance:
            return PredicatesRecommender.__instance

        return PredicatesRecommender()

    def recommend(self, q):
        vectorized_text = self.vectorize_input(q)

        comparison_ids = self.predict_comparisons(vectorized_text)
        return self.map_to_predicates(comparison_ids)

    def predict_comparisons(self, vectorized_text):
        cluster_label = self.model.predict(vectorized_text)
        cluster_instances_indices = np.argwhere(self.model.labels_ == cluster_label).squeeze(1)
        cluster_instances = self.train_df.iloc[cluster_instances_indices]
        comparison_ids = cluster_instances['comparison_id'].unique()
        return comparison_ids

    def map_to_predicates(self, comparison_ids):
        predicate_ids = []
        predicates = []

        for comparison_id in comparison_ids:
            for predicate in self.comparisons[comparison_id]:
                if predicate['id'] in predicate_ids:
                    continue

                predicate_ids.append(predicate['id'])
                predicates.append(predicate)

        return predicates

    def read_training_data(self):
        train_json = IO.read_json(self.TRAINING_SET_PATH)
        train_df = pd.json_normalize(train_json['instances'])

        return train_df

    def vectorize_input(self, q):
        preprocessed_text_arr = text_process(q)
        preprocessed_text_str = ' '.join(preprocessed_text_arr)
        text_vectorized = self.vectorizer.transform([preprocessed_text_str])
        return text_vectorized
