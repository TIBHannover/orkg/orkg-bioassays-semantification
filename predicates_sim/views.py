from flask import jsonify, request
from flask.views import MethodView

from common.errors import ValidationError
from services.metadata import MetadataService
from ._validator import Validator
from .orkg_predicates import PredicatesRecommender

metaService = MetadataService.get_instance()
recommender = PredicatesRecommender.get_instance()


class RequestHandler:

    @staticmethod
    def handle(request):
        abstract = metaService.by_doi(request.args.get('doi')) or metaService.by_title(request.args.get('title'))
        q = '{} {}'.format(request.args.get('title'), abstract)

        return q


class PredicatesSimAPI(MethodView):

    def get(self, **kwargs):

        try:
            Validator.validate_request(request.args)
        except ValidationError as e:
            return e.message, e.status

        q = RequestHandler.handle(request)

        predicates = recommender.recommend(q)

        if not predicates:
            return jsonify([])

        return jsonify(predicates)
