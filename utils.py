import pickle
import json

from webargs.flaskparser import use_args


class IO:

    @staticmethod
    def read_pickle(input_path):
        with open(input_path, 'rb') as f:
            loaded_object = pickle.load(f)
        return loaded_object

    @staticmethod
    def read_json(input_path):
        with open(input_path, encoding='utf-8') as f:
            json_data = json.load(f)

        return json_data


def append_value(dict_obj, key, value):
    if key in dict_obj:
        if not isinstance(dict_obj[key], list):
            dict_obj[key] = [dict_obj[key]]
        dict_obj[key].append(value)
    else:
        dict_obj[key] = value


def use_args_with(schema_cls, schema_kwargs=None, **kwargs):
    schema_kwargs = schema_kwargs or {}

    def factory(request):
        # Filter based on 'fields' query parameter
        only = request.args.get("fields", None)
        # Respect partial updates for PATCH requests
        partial = request.method == "PATCH"
        # Add current request to the schema's context
        # and ensure we're always using strict mode
        return schema_cls(
            only=only, partial=partial, context={"request": request}, **schema_kwargs
        )

    return use_args(factory, **kwargs)
